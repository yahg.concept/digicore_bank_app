package com.example.simplebankapp.service;
import com.example.simplebankapp.dto.RegistrationDto;

public interface CreateNewUserService {
    public void createUser(RegistrationDto registrationDto);

}

package com.example.simplebankapp.service;

public interface CustWithdrawalService {
    public void withdraw(Double withdrawAmount);
}

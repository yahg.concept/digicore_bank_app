package com.example.simplebankapp.service.serviceimplementation;

public interface DepositService {
    void deposit (Double depositAmount);

}
